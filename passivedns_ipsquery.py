#!/usr/bin/python
# this script parses json responses from api.cyberwatchfloor.com
# Version 0.1
# program name: passivedns_ipsquery.py

import sys 
import string
import getpass
import argparse
import requests
from requests.auth import HTTPDigestAuth
import json

# Set the API URL for the cyberwatchfloor authentication
token_url = "https://api.cyberwatchfloor.com/api/v1/token"

# Set the API URL for domain query
ips_url = "https://api.cyberwatchfloor.com/api/v1/ips"

# Parse the arguments provided by the command line
parser = argparse.ArgumentParser(description='Perform a passive DNS query of api.cyberwatchfloor.com for a set of IP addresses.')

# Parse debug flag
parser.add_argument('--debug', action='store_true', help='Enable debugging output')

# Parse the API Token
parser.add_argument('-a',help='The API Authentication Token')

# Parse the DNS query type
parser.add_argument('-q', help='Hostname to query.')

#Parse the size of the query max is 100K, default is 100
parser.add_argument('-s', type=int, help='Number of results, max 100K to return.')

# Parse the offset size into the result set, useful for parsing more than 100K results
parser.add_argument('-o', type=int, help='Offset into the result set, for example 100001 to go beyond the 100K results limitation on returns')

# Parse the begin date for the query
parser.add_argument('-b', help='Begin date for query, format 2014-05-06T12:51:20Z')

# Parse the end date for the query
parser.add_argument('-e', help='End date for query, format 2014-05-06T12:51:20Z')

# Parse param for verbose  mode, just output only, no headers, etc
parser.add_argument('-v', action='store_true', help='Verbose mode, print number of results and output headers')

args = parser.parse_args()
#Print out the args for execution debugging
if (args.debug):
	print "The Authtoken is", args.a
	print "The IP(s) are", args.q
	print "The result set size is", args.s
	print "The offset is", args.o


# Request the user to enter their credentials if no access token provided
if (args.a is None):

	# Request user input for username and password to obtain authentication token for API
	authResponse = requests.post(token_url,data={'username':raw_input("Username: "), 'password':getpass.getpass('Password: ')})
	#print (authResponse.status_code)

	if (authResponse.ok):
		json_auth_response = json.loads(authResponse.content)
		current_auth_token = json_auth_response['token']
		print "API Auth Token for this session:", current_auth_token

	else:
	   authResponse.Raise_for_Status()
else:
	current_auth_token = args.a

# Create the payload for the query GET Request
# Check to see if size was specified, if not go with 100 which is API default
if (args.s is not None):
	qSize = args.s
	qPayload = {'q': args.q, 'token':current_auth_token, 'size':qSize}
else:
	qSize = 100
	qPayload = {'q': args.q, 'token':current_auth_token}

# Add the query result offset to the GET payload
if (args.o is not None):
	qPayload['start'] = args.o

# Add the begin and/or end date to the GET payload
if (args.b is not None):
	qPayload['date_start'] = args.b
if (args.e is not None):
	qPayload['date_end'] = args.e

# Query the IPs in passive DNS using the API
qResponse = requests.get(ips_url, params=qPayload)
if (args.debug):
	print "The URL request to the API was", qResponse.url, '\n'
	print "The content", qResponse.content, '\n'

#Parse the json return
qJson = json.loads(qResponse.content)

#Print the number of results found and returned
if (args.v): print qJson['info'];

#parse out the size of the result set
rSize = int((qJson['info'].split())[0])
if (args.debug):
	print "Total results found =", rSize
	print "Actual number of results requested=", qSize

#gracefully exit when no results are returned
if (rSize == 0):
	sys.exit(0)
elif (rSize < qSize):
	qSize = rSize


# Print the results of the domain query in the following format
# query name, value, type
if (args.debug): print "The final processed query results";
# Print the header for results
if (args.v): 
	print "Date, Hostname Queried, Query Answer, DNS Query Type (Numeric), DNS Query Type (Text)"

count = 0 
while (count < qSize ):
	print qJson['results'][count]['date'].encode('ascii', errors='ignore'),",",
	print qJson['results'][count]['qname'].encode('ascii', errors='ignore'),",",
	print qJson['results'][count]['value'].encode('ascii', errors='ignore'),",",
#	print qJson['results'][count]['value_ip'].encode('ascii', errors='ignore'),",",
	print qJson['results'][count]['qtype'],",",
	print qJson['results'][count]['type']
	count += 1

#exit return 1 for programmatic checks for success.
sys.exit(1)

